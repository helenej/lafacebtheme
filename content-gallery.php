<h2 class="mbn ptl txtcenter">L'aperçu</h2>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <article>
        <?php the_content(); ?>
    </article>

<?php endwhile; ?>
<?php endif; ?>

<div class="owl-carousel owl-theme pal">
<?php
// Create a stream
$opts = array(
  'http'=>array(
    'method'=>"GET"
  )
);

$context = stream_context_create($opts);
$base_url = 'https://api.instagram.com/v1/';
$count = 10;

// Open the file using the HTTP headers set above
$file = file_get_contents($base_url . 'users/3928545381/media/recent/?count=' . $count . '&access_token=' . get_option('token-instagram'), false, $context);
if ($file) {
    $json = json_decode($file);
    foreach ($json->data as $media) {
        $image = $media->images->low_resolution;
        $url = $image->url;
?>
    <div>
        <a href="<?php echo $media->link; ?>"><img class="owl-lazy" data-src="<?php echo $url; ?>" alt="<?php echo $media->caption->text; ?>"></img></a>
    </div>
<?php
    }
}
?>
</div>
