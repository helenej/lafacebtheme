<?php
$defaults = array(
	'theme_location'  => 'main_menu',
	'container_class' => 'menu-container grid',
	'menu_class'      => 'menu mtl mrl small-mtn flex-container-small-v',
	'fallback_cb'     => false, // No default menu
	'items_wrap'      => '<div class="logo-container mtm mlm one-fifth"><a href="/"><img src="' . get_template_directory_uri() . '/images/logo_icon.png" alt="logo" /></a></div><ul id="%1$s" class="%2$s">%3$s</ul>',
    'walker'		  => new Walker_OnePage_Menu()
);

wp_nav_menu( $defaults );