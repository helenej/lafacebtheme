 <footer class="pal small-pas">
  <div class="pal small-pas grid has-gutter-xl">
   <div class="one-third">
    <iframe width="100%" height="100%" frameborder="0" style="border:0"
    src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJ56XIwJy8rhIRL6gfNW0wKEM&key=AIzaSyARJnIuY7klOiof397O4_dovif0R33LYSc" allowfullscreen></iframe>
   </div>
   <div class="ptl pbl one-half">
    <p><?php echo get_option('info-address'); ?></p>
    <p><?php echo get_option('info-hours'); ?></p>
    <p><a href="tel:<?php echo str_replace(' ', '', get_option('info-phone')); ?>"><?php echo get_option('info-phone'); ?></a></p>
    <p><a href="mailto:<?php echo get_option('info-email'); ?>?Subject=Site%20web" target="_top"><?php echo get_option('info-email'); ?></a></p>
    <p>
     <span class="heading-3">Suivez-nous</span>
     <br class="show-for-mobile" />
     <a href="https://<?php echo get_option('info-facebook'); ?>" target="_blank" rel="noopener"><img class="logo-tier" src="<?php bloginfo('template_url'); ?>/images/logo_facebook.png" alt="Facebook" /></a>
     <a href="https://<?php echo get_option('info-instagram'); ?>" target="_blank" rel="noopener"><img class="logo-tier" src="<?php bloginfo('template_url'); ?>/images/logo_instagram.png" alt="Instagram" /></a>
     <a href="https://<?php echo get_option('info-tripadvisor'); ?>" target="_blank" rel="noopener"><img class="logo-tier" src="<?php bloginfo('template_url'); ?>/images/logo_tripadvisor.png" alt="Tripadvisor" /></a>
    </p>
   </div>
   <?php get_sidebar(); ?>
  </div>

  <div class="copyright small">Tous droits réservés - <?php echo date("Y"); ?> - La Face B</div>

  <?php wp_footer(); ?>
 </footer>
</div> <!-- end of main_wrapper -->
</body>
</html>
