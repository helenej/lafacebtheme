<h2 class="reservation-heading w70 small-w80">Votre réservation</h2>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<article>
    <?php the_content(); ?>
</article>

<?php endwhile; ?>
<?php endif; ?>
