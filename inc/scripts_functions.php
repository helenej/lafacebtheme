<?php

/**
 * Load JS libraries and scripts.
 */
function lafaceb_scripts() {

    $extension = '.js';
    if (!SCRIPT_DEBUG) {
        $extension = '.min.js';
    }

    wp_register_script( 'parallax', get_stylesheet_directory_uri() . '/js/parallax.min.js', array('jquery'), '1.5.0', true );
    wp_enqueue_script('parallax');
    wp_register_style( 'owl-carousel-style', get_stylesheet_directory_uri() . '/js/owl.carousel.min.css' );
    wp_enqueue_style('owl-carousel-style');
    wp_register_style( 'owl-carousel-theme-style', get_stylesheet_directory_uri() . '/js/owl.theme.default.min.css' );
    wp_enqueue_style('owl-carousel-theme-style');
    wp_register_script( 'owl-carousel', get_stylesheet_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '2.3.3', true );
    wp_enqueue_script('owl-carousel');

    // Enqueue a script that has jquery as dependency
    wp_enqueue_script('init-js', get_stylesheet_directory_uri() . '/js/init.js', array('jquery'), '1.0', true);
}
