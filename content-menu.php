<div class="banner mtl small-mtn flex-container-centered" data-parallax="scroll" data-image-src="<?php bloginfo('template_url'); ?>/images/banner/cafe_gourmand.jpg">
    <h2>Le menu</h2>
</div>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div class="mtl small-mtn pal small-pas grid-2-small-1">
    <div class="pal small-pas txtcenter">
        <p class="heading-1">Fait maison</p>
        <article>
            <?php the_content(); ?>
        </article>

        <?php endwhile; ?>
        <?php endif; ?>

<?php
$menu_pdf = get_attached_media('application/pdf');
if ($menu_pdf) {
?>
        <a class="button w100 mtl small-mts" href="<?php echo wp_get_attachment_url(array_pop($menu_pdf)->ID) ?>">Voir le menu</a>
<?php
}
?>

        <p class="mtl small-mts">
            <span class="heading-2">Commander</span><br class="show-for-mobile" />
            <a href="https://<?php echo get_option('info-ubereats'); ?>" target="_blank" rel="noopener"><img class="logo-tier" src="<?php bloginfo('template_url'); ?>/images/logo_ubereats.png" alt="Uber Eats"></img></a>
            <a href="https://<?php echo get_option('info-deliveroo'); ?>" target="_blank" rel="noopener"><img class="logo-tier" src="<?php bloginfo('template_url'); ?>/images/logo_deliveroo.png" alt="Deliveroo"></img></a>
        </p>
    </div>

    <div class="pal small-pas txtcenter">
        <img class="img-shadow" src="<?php bloginfo('template_url'); ?>/images/interieur.jpg" alt="Intérieur"></img>
    </div>
</div>
