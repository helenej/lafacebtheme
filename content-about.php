<div class="banner txtcenter mtl small-mtn flex-container-centered" data-parallax="scroll" data-image-src="<?php bloginfo('template_url'); ?>/images/banner/salade.jpg">
    <h2>Notre histoire</h2>
</div>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div class="mtl small-mtn pal small-pas grid-2-small-1">
    <div class="pal small-pas txtcenter">
        <img class="img-shadow" src="<?php bloginfo('template_url'); ?>/images/facade.jpg" alt="Façade"></img>
    </div>

    <div class="pal small-pas txtcenter">
        <p class="heading-1">À propos</p>
        <article>
            <?php the_content(); ?>
        </article>

        <?php endwhile; ?>
        <?php endif; ?>
    </div>
</div>
