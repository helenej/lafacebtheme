<div class="ptl pbl">
    <div class="txtcenter">
        <p class="heading-1">Découvrez</p>
        <h2 class="mtn">Nos événements</h2>
    </div>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <article>
        <?php the_content(); ?>
    </article>

<?php endwhile; ?>
<?php endif; ?>

<?php
// Create a stream
$opts = array(
  'http'=>array(
    'method'=>"GET"
  )
);

$context = stream_context_create($opts);
$base_url = 'https://graph.facebook.com/v2.10/';
$token = get_option('token-facebook');

// Open the file using the HTTP headers set above
$file = file_get_contents($base_url . '1490843381244936/events?time_filter=upcoming&access_token=' . $token, false, $context);
if ($file && json_decode($file)->data) {
    $json = json_decode($file);
    foreach ($json->data as $event) {
        $file = file_get_contents($base_url . $event->id . '?fields=cover&access_token=' . $token, false, $context);
        $json = json_decode($file);
        $date = date_parse($event->start_time);
?>
    <div class="evenement w70 mbl grid center">
        <div class="one-third flex-container-centered">
            <img src="<?php echo $json->cover->source; ?>" alt="Visuel de l'événement"></img>
        </div>
        <div class="pam flex-container-v">
            <h3 class="mbn"><?php echo $event->name; ?></h3>
            <p class="flex-item-fluid"><?php echo substr($event->description, 0, strpos(wordwrap($event->description), "\n")); ?>...</p>
            <a href="https://fr-fr.facebook.com/events/<?php echo $event->id; ?>/">Détails</a>
        </div>
        <div class="flex-container-v one-fifth">
            <div class="flex-item-center">
                <?php echo $date['day'] . '.' . $date['month']; ?><br /><?php echo $date['year']; ?>
            </div>
        </div>
    </div>
<?php
    }
} else {
    $file = file_get_contents($base_url . '1490843381244936/events?limit=1&time_filter=past&access_token=' . $token, false, $context);
    if ($file) {
        $json = json_decode($file);
        if (!empty($data)) {
            $event = $json->data[0];
            $file = file_get_contents($base_url . $event->id . '?fields=cover&access_token=' . $token, false, $context);
            $json = json_decode($file);
            $date = date_parse($event->start_time);
?>
    <p class="heading-3 txtcenter">Pas d'événement prévu pour le moment.</p>
    <div class="evenement w70 mtl small-mts mbl small-mbs grid center">
        <div class="one-third flex-container-centered">
            <img src="<?php echo $json->cover->source; ?>" alt="Visuel de l'événement"></img>
        </div>
        <div class="pam flex-container-v">
            <h3 class="mbn"><?php echo $event->name; ?></h3>
            <p class="flex-item-fluid"><?php echo substr($event->description, 0, strpos(wordwrap($event->description), "\n")); ?>...</p>
            <a href="https://fr-fr.facebook.com/events/<?php echo $event->id; ?>/">Détails</a>
        </div>
        <div class="flex-container-v one-fifth">
            <div class="flex-item-center">
                Passé
            </div>
        </div>
    </div>
<?php
        }
        else {
?>
<div class="txtcenter">
    <a class="logo-button" href="https://<?php echo get_option('info-facebook'); ?>/events" target="_blank" rel="noopener">&#xea90;</a>
</div>
<?php
        }
    }
}
?>
</div>
