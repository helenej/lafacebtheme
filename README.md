Customize General Settings (Contact Info and Access Token).

Create a new page for a new section.
Available sections (corresponding page name):

* Réservation: insert shortcode from The Fork Plugin
* Histoire
* Menu: upload a PDF menu
* Evénements: acces token required
* Privatisation
* Galerie: acces token required

Create a navigation menu.