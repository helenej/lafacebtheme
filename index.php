<?php get_header(); ?>

<?php get_template_part('menu') ?>

<?php

$page_map = array(
    "Réservation"   => "booking",
    "Histoire"      => "about",
    "Menu"          => "menu",
    "Evénements"    => "events",
    "Privatisation" => "privatisation",
    "Galerie"       => "gallery"
    );

foreach ( $page_map as $title => $id ) {
    $page = get_page_by_title( $title );
    if ($page) {
        $args = array(
            'p' => $page->ID,
            'post_type' => 'any'
            );
        $wp_query = new WP_Query( $args );
        if ( 'publish' === get_post_status( $wp_query->get_queried_object_id() ) ) {
?>

<section id="<?php echo sanitize_title($title) ?>">
    <?php get_template_part('content', $id) ?>
</section>

<?php
        }
        wp_reset_query();
    }
}
?>

<div id="scroll-top">
    <a class="button" href="#">&#xea41;</a>
</div>

<?php get_footer(); ?>
