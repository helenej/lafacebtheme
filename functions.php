<?php

// Hook into some actions and call some functions when they fire

// Styles and scripts
add_action( 'wp_enqueue_scripts', 'lafaceb_add_theme_scripts' );
function lafaceb_add_theme_scripts() {
    wp_enqueue_style( 'knacss', get_stylesheet_directory_uri() . '/knacss.css' );
    wp_enqueue_style( 'style', get_stylesheet_uri() );
}

// La Face B setup
add_action('after_setup_theme', 'lafaceb_setup');
function lafaceb_setup()
{
    locate_template(array('inc/scripts_functions.php'), true, true);

    load_theme_textdomain('lafaceb', get_template_directory() . '/languages'); // Load translation

    add_theme_support('post-thumbnails'); // Enable thumbnails
}

// Register menu
add_action('init', 'lafaceb_add_menu');
function lafaceb_add_menu()
{
    register_nav_menu('main_menu', 'Menu principal');
}

// Add custom classes to menu items
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item)
{
	$classes[] = "smooth-scroll";
	return $classes;
}

/* Menu Custom Walker */
class Walker_OnePage_Menu extends Walker_Nav_Menu {

    /**
     *
     *
     * Note: Menu objects include url and title properties, so we will use those.
     */
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

        parent::start_el($output, $item, $depth, $args, $id);

        $url_redirected = strtok($item->url, '?') . "#" . sanitize_title(get_the_title( $item->object_id ));

        // extract last href
        $last_index_of_href = strrpos($output, "href");
        $after_href_str = substr($output, $last_index_of_href);

        // add anchor text
        $after_href_str = preg_replace('/href="(.*)"/', 'href="${1}' . '#' . sanitize_title(get_the_title( $item->object_id )) . '"', $after_href_str);

        // rebuild string
        $output = substr($output, 0, $last_index_of_href) . $after_href_str;
    }

}

add_action('wp_enqueue_scripts', 'lafaceb_scripts');

/* Register sidebars */
add_action('widgets_init','lafaceb_add_sidebars');
function lafaceb_add_sidebars()
{
    register_sidebar(array(
    'id' => 'widget_area_bottom',
    'name' => __('Widgets area at the bottom', 'lafaceb'),
    'description' => __('Appears in the footer', 'lafaceb'),
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
            ));
}

add_action( 'admin_init', 'lafaceb_register_settings' );

/* Register settings */
function lafaceb_register_settings()
{
    add_settings_section(
        'site-contact-info',
        __('Contact Information', 'lafaceb'),
        'lafaceb_section_callback',
        'general'
    );
    add_settings_section(
        'site-access-token',
        __('Access token', 'lafaceb'),
        'lafaceb_section_callback',
        'general'
    );
    add_settings_field(
        'info-phone',
        __('Phone Number', 'lafaceb'),
        'lafaceb_print_text_field',
        'general',
        'site-contact-info',
        array(
            'info-phone'
        )
    );
    add_settings_field(
        'info-email',
        __('Email Address', 'lafaceb'),
        'lafaceb_print_text_field',
        'general',
        'site-contact-info',
        array(
            'info-email'
        )
    );
    add_settings_field(
        'info-address',
        __('Street Address', 'lafaceb'),
        'lafaceb_print_text_field',
        'general',
        'site-contact-info',
        array(
            'info-address'
        )
    );
    add_settings_field(
        'info-hours',
        __('Hours', 'lafaceb'),
        'lafaceb_print_text_field',
        'general',
        'site-contact-info',
        array(
            'info-hours'
        )
    );
    add_settings_field(
        'info-ubereats',
        __('Uber Eats Link', 'lafaceb'),
        'lafaceb_print_text_field',
        'general',
        'site-contact-info',
        array(
            'info-ubereats'
        )
    );
    add_settings_field(
        'info-deliveroo',
        __('Deliveroo Link', 'lafaceb'),
        'lafaceb_print_text_field',
        'general',
        'site-contact-info',
        array(
            'info-deliveroo'
        )
    );
    add_settings_field(
        'info-facebook',
        __('Facebook Link', 'lafaceb'),
        'lafaceb_print_text_field',
        'general',
        'site-contact-info',
        array(
            'info-facebook'
        )
    );
    add_settings_field(
        'info-instagram',
        __('Instagram Link', 'lafaceb'),
        'lafaceb_print_text_field',
        'general',
        'site-contact-info',
        array(
            'info-instagram'
        )
    );
    add_settings_field(
        'info-tripadvisor',
        __('Tripadvisor Link', 'lafaceb'),
        'lafaceb_print_text_field',
        'general',
        'site-contact-info',
        array(
            'info-tripadvisor'
        )
    );
    add_settings_field(
        'token-facebook',
        __('Facebook Access Token', 'lafaceb'),
        'lafaceb_print_text_field',
        'general',
        'site-access-token',
        array(
            'token-facebook'
        )
    );
    add_settings_field(
        'token-instagram',
        __('Instagram Access Token', 'lafaceb'),
        'lafaceb_print_text_field',
        'general',
        'site-access-token',
        array(
            'token-instagram'
        )
    );
    register_setting('general','info-phone', 'esc_attr');
    register_setting('general','info-email', 'esc_attr');
    register_setting('general','info-address', 'esc_attr');
    register_setting('general','info-hours', 'esc_attr');
    register_setting('general','info-ubereats', 'esc_attr');
    register_setting('general','info-deliveroo', 'esc_attr');
    register_setting('general','info-facebook', 'esc_attr');
    register_setting('general','info-instagram', 'esc_attr');
    register_setting('general','info-tripadvisor', 'esc_attr');
    register_setting('general','token-facebook', 'esc_attr');
    register_setting('general','token-instagram', 'esc_attr');
}

function lafaceb_section_callback() // Section Callback
{
    return false;
}

/* Print settings field content */
function lafaceb_print_text_field($args) // Textbox Callback
{
    $option = get_option($args[0]);
    echo '<input type="text" id="'. $args[0] .'" name="'. $args[0] .'" value="' . $option . '" size="100" />';
}
