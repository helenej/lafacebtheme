<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php wp_head(); ?>
</head>
<body>
 <div id="main_wrapper"> <!-- End tag is in footer -->
  <header class="flex-container-v header-small">
   <div class="header-slideshow" data-parallax="scroll" data-image-src="<?php bloginfo('template_url'); ?>/images/slideshow/bar.jpg"></div>
  </header>