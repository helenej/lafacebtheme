<?php
/*
 Template Name: Simple page
*/
?>
<?php get_header('small'); ?>

<?php get_template_part('menu') ?>

<h2 class="reservation-heading w70 small-w80"><?php the_title(); ?></h2>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<article class="mtl small-mtn pal small-pas">
    <?php the_content(); ?>
</article>

<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>