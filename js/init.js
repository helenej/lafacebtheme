/* global jQuery */

jQuery(function($) {
	// Responsive menu
	$('.menu-container').click(function() {
		$('.menu-container').toggleClass('expand');
	});

	// Smooth scroll
	var speed = 750;
	$('.smooth-scroll').on('click', function() {
		var page = $(this).find('a').attr('href');
		var hash = page.substring(page.indexOf('#'));
		$('html, body').animate({
			scrollTop: $(hash).offset().top
		}, speed); // Go
		return false;
	});

	// Scroll to top button
	$(window).scroll(function() {
		if ($(this).scrollTop() > 120) {
			// When the user scrolls down 120px (menu height) from the top of the document, show the button
			$('#scroll-top .button').show();
		}
		else {
			$('#scroll-top .button').hide();
		}
	});
	$('#scroll-top .button').click(function() {
		$('html, body').animate({
			scrollTop: 0
		}, speed);
		return false;
	});

	// Gallery carousel
	var owl = $('.owl-carousel');
	owl.owlCarousel({
		autoplay: true,
		lazyLoad: true,
		loop: true,
		margin: 20,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 2
			},
			1000: {
				items: 4
			}
		}
	});
	// Make images square
	$('.owl-item img').height($('.owl-item img').width()).css({
		"object-fit": "cover"
	});
});
